from application import api

from .auth import api as auth_ns
from .account import api as account_ns

api.add_namespace(auth_ns)
api.add_namespace(account_ns)
