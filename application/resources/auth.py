from flask import request, g, abort, render_template
from config import APP_NAME
from application import auth, Resource, db
from application.database import User, ResetToken
from application.custom.email import send_email
from flask_restplus import Namespace


api = Namespace("Authentication", "Users authentication using a custom HTTPBasicAuth", path='/auth')


@api.route('/token')
class LoginResource(Resource):
    @auth.login_required
    def get(self):
        return {
            "token": g.user.token.decode('utf-8')
        }


@api.route('/token')
class LogoutResource(Resource):
    @auth.login_required
    def delete(self, **params):
        # TODO: delete session token
        print(g.user)
        return {"message": "TODO: how to delete a token?"}


@api.route('/password/reset')
class ResetPassword(Resource):
    def post(self, email=None):
        """
        Given an email, ask to receive a password reset token by email.
        """
        user = User.query.filter_by(email=email).first()
        if user:
            rt = ResetToken(user.id)
            db.session.add(rt)
            db.session.commit()
            result = send_email(user.email, "%s - reset password"%APP_NAME,
                "mail/reset_password.html", reset_url="http://localhost/#!/password/change?token=%s"%rt.token)
            return result
        else:
            return {
                "success": False,
                "message": "No registered user found with this email address"
            }, 404


@api.route('/password/change')
class ChangePassword(Resource):
    def put(self, **params):
        rt = ResetToken.query.filter_by(token=params['token']).first()
        if not rt:
            return {"success": False, "message": "Invalid token, please ask a new reset token and try again."}, 403
        user = User.query.filter_by(id=rt.user_id).first()
        user.password = params['password']
        db.session.commit()
        return {"success": True, "message": "Password successfully changed!"}


@api.route('/account/remove')
class RemoveAccount(Resource):
    def delete(self):
        """Remove your own account"""
        return ""

@api.route('/account/<int:id>/remove')
class RemoveAccountAdmin(Resource):
    def delete(self, id=None):
        """(Admin only) remove a user's account"""
        return ""