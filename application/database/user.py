import enum
from flask import g, abort
from config import SECRET_KEY
from application import db, ma, auth
from sqlalchemy.ext.hybrid import hybrid_property
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
import random
from datetime import datetime

class ResetToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    token = db.Column(db.String, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)

    def __init__(self, user_id):
        self.user_id = user_id
        self.token = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWYZ') for _ in range(30))
        self.created_at = datetime.now()


# define the structure of our user object
from .group import group
from sqlalchemy_enum_list import EnumListType
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=True, unique=True)
    email = db.Column(db.String(50), unique=True)
    pwhash = db.Column(db.String(128))
    _groups = db.Column(EnumListType(group, str))

    def has_group(self, name_or_enum):
        return group(name_or_enum) in self.groups

    @hybrid_property
    def groups(self):
        return self._groups

    @groups.setter
    def groups(self, group_names):
        self._groups = [group(name) for name in group_names]

    @hybrid_property
    def password(self):
        return None

    @password.setter
    def password(self, pwd):
        self.pwhash = pwd_context.encrypt(pwd)

    @hybrid_property
    def token(self):
        s = Serializer(SECRET_KEY, expires_in=300)
        return s.dumps({'id': self.id})

    def verify_password(self, password):
        return pwd_context.verify(password, self.pwhash)

    @classmethod
    def verify_auth_token(cls, token):
        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = cls.query.get(data['id'])
        return user

    @classmethod
    def login(cls, email, password):
        """
        Password is optionnal if loging with token
        """
        user = cls.query.filter_by(email=email).first()
        if not user or not user.verify_password(password):
            user = None
        return user


# schema explaining how to dump a user
class UserSchema(ma.ModelSchema):
    class Meta:
        fields = ('id', 'username')
        model = User
        strict = True


User.Schema = UserSchema

# user authentication using existing token
@auth.verify_password
def check_password(email_or_token, password=None):
    user = User.verify_auth_token(email_or_token)
    if not user:
        user = User.login(email_or_token, password)
    g.user = user
    return user!=None

from functools import wraps
def group_required(group):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            has_group = group in [ group.name for group in g.user.groups ]
            if not has_group:
                abort(403)
            return func(*args, **kwargs)
        return auth.login_required(wrapper)
    return decorator

auth.group_required = group_required
