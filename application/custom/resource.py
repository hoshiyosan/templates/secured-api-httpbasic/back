from flask import request
from functools import wraps
import flask_restplus as fr

def inject_kwargs(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        params = request.get_json() or {}
        kwargs = {**params, **kwargs}
        return func(*args, **kwargs)
    return wrapper

def serialize_sqlalchemy(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        to_serialize = func(*args, **kwargs)
        try:
            data = to_serialize.Schema(many=False).dump(to_serialize).data
            return data
        except AttributeError:
            if len(to_serialize)>0:
                return to_serialize[0].Schema(many=True).dump(to_serialize).data
            else:
                return []
    return wrapper


class Resource(fr.Resource):
    method_decorators = [inject_kwargs]

class DBResource(Resource):
    method_decorators = [inject_kwargs, serialize_sqlalchemy]
