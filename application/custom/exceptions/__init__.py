from sqlalchemy.exc import SQLAlchemyError

class ExceptionsHandler:
    def __init__(self, app=None):
        if app:
            self.init_app(app)

    def init_app(self, app):
        from application.custom.exceptions.global_handler import global_handler
        from application.custom.exceptions.sqlalchemy_handler import sqlalchemy_handler

        app.register_error_handler(Exception, global_handler)
        app.register_error_handler(SQLAlchemyError, sqlalchemy_handler)
