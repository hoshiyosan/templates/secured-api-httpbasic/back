from flask import jsonify
from application import db
from application import logger
from application.custom.exceptions.global_handler import global_handler

def sqlalchemy_handler(error=None):
    db.session.rollback()
    logger.info("session has been rollback due to the following exception")
    return global_handler(error)
